import Vue from "vue";
import VueRouter from "vue-router";
//import Home from "../views/Home.vue";
import Home from "@/pages/PageHome"
import ThreadShow from "@/pages/PageThreadShow"
import PageNotFound from "@/pages/PageNotFound"
import PageChannel from "@/pages/PageChannel"

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/thread/:id",
    name: "ThreadShow",
    component: ThreadShow,
    props: true
  },
  {
    path: "/channel/:id",
    name: "Channel",
    component: PageChannel,
    props: true
  },
  {
    path: "*",
    name: "NotFound",
    component: PageNotFound
  }
]
const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;
