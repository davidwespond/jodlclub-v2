import Vue from "vue";
import Vuex from "vuex";
import sourceData from "@/data.json"
Vue.use(Vuex);

export default new Vuex.Store({
  state: sourceData,
  mutations: {
    setPost (state, {post, postID}) {
      Vue.set(state.posts, postID, post) // set post
    },
    appendPostToThread (state, {postID, threadID}) {
      const thread = state.threads[threadID]
      Vue.set(thread.posts, postID, postID) // append post to thread
    },
    appendPostToUser (state, {postID, userID}) {
      const user = state.users[userID]
      Vue.set(user.posts, postID, postID) // append post to user
    }
  },
  actions: {
    createPost (context, post) {
      const postID = 'postNR'+ Math.random()
      post['.key'] = postID
      context.commit('setPost', {post, postID})
      context.commit('appendPostToThread', {threadID: post.threadID, postID})
      context.commit('appendPostToUser', {userID: post.userID, postID})
    }
  },
  modules: {}
});
